import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Image, Text, View } from "react-native";

//Importando páginas

import Login from "./src/login";
import Cadastro from "./src/cadastro";
import Home from "./src/home";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="Login"
          component={Login}
          options={{

            headerTitle: (props) => (
              //View usada para agrupar a imagem e o texto do título
              <View
                style={{  //Estilização da view
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                {/* Logo do aplicativo */}
                <Image
                  style={{ width: 30, height: 30, marginRight: 4 }} //Estilização da imagem
                  source={require("./src/img/logotipo.png")}  //Caminho da imagem
                  resizeMode="contain"
                />

                {/* Título com o nome do aplicativo */}
                <Text style={{ fontSize: 16, fontWeight: "bold" }}>
                  RGT
                </Text>
              </View>
            ),
          }}
        />

        <Stack.Screen
          name="Cadastro"
          component={Cadastro}
          options={{ title: "" }} // Deixando o titúlo vazio
        />

        {/* Págia home apenas exemplo */}
        <Stack.Screen 
        name="Home" 
        component={Home} 
        />

      </Stack.Navigator>
    </NavigationContainer>
  );
}
