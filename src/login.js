import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import React, { useState } from "react";

const Login = ({ navigation }) => {
  //Definindo valores para email e senha
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");

  // Função para navegar para a página Cadastro
  const handleCadastro = () => {
    navigation.navigate("Cadastro");
  };

  //Função para simular a validação que seria feito na API. 
  const handleLogar = () => {
    if (email == "teste@teste" && senha == "1234") { // Email e senha de teste definidos 
      navigation.navigate("Home");
    } else {
      alert("Usuário ou senha incorreto");
    }
  };

  return (
    <View style={styles.container}>
      {/* Texto Login */}
      <Text style={styles.textoLogin}>Login</Text>

      {/* Campos de texto e-mail e senha */}
      <TextInput
        style={styles.inputEmail}
        placeholder="E-mail"
        value={email}
        onChangeText={(text) => {
          setEmail(text);
        }}
      ></TextInput>
      <TextInput
        style={styles.inputSenha}
        placeholder="Senha"
        value={senha}
        onChangeText={(text) => {
          setSenha(text);
        }}
      ></TextInput>

      {/* Botão para navegar para a página de cadastro */}
      <TouchableOpacity onPress={handleCadastro}>
        <Text>Criar uma nova conta</Text>
      </TouchableOpacity>

      {/* Botão de login para navegar para a página home */}
      <TouchableOpacity style={styles.botaoEntrar} onPress={handleLogar}>
        <Text style={styles.textoBotaoEntrar}>Entrar</Text>
      </TouchableOpacity>
    </View>
  );
};

//Estilização

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#fff",
    height: "100%",
    width: "100%",
  },
  textoLogin: {
    fontSize: 28,
    marginTop: 60,
    marginBottom: 70,
  },
  inputEmail: {
    borderWidth: 0.5,
    borderRadius: 3,
    width: 300,
    height: 45,
    marginBottom: 40,
    paddingLeft: 5,
  },
  inputSenha: {
    borderWidth: 0.5,
    borderRadius: 3,
    width: 300,
    height: 45,
    paddingLeft: 5,
    marginBottom: 5,
  },
  botaoEntrar: {
    width: 300,
    height: 55,
    marginTop: 100,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    borderWidth: 1,
    backgroundColor: "orange",
  },
  textoBotaoEntrar: {
    fontSize: 20,
  },
});

export default Login;
