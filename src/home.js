import { View, Text, StyleSheet } from "react-native";
import React from "react";

//Página de exemplo para acessar depois de logar

const Home = () => {
  return (
    <View style={styles.home}>
      <Text style={styles.texto}>Página Home</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  home: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  texto: {
    fontSize: 22,
  },
});

export default Home;
