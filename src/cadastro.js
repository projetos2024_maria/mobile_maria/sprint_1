import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from "react-native";
import React, { useState } from "react";

const Cadastro = ({ navigation }) => {
  //Definindo valores
  const [nome, setNome] = useState("");
  const [telefone, setTelefone] = useState("");
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");
  const [confirmarSenha, setConfirmarSenha] = useState("");

  //Função para simular as validações que seriam feitas na API 
  const handleCadastrar = () => {
    if (nome != "" && email != "" && senha != "" && confirmarSenha != "") { 
      let telefoneText = telefone.toString(); 
      let telefoneLength = telefoneText.length; 
      if (telefoneLength != 11) { 
        alert("Formato do telefone está incorreto");
      } else if (senha != confirmarSenha) {  
        alert("As senhas informadas não coincidem");
      } else { 
        alert("Nova conta cadastrada com sucesso");
        navigation.goBack();
      }
    } else { 
      alert("Preencha todos os campos");
    }
  };

  return (
    <View style={styles.container}>
      {/* Texto Nova Conta */}
      <Text style={styles.textoCadastrar}>Nova Conta</Text>

      {/* Campos de texto nome, cpf, e-mail, senha e confirmar senha */}
      <TextInput
        style={styles.input}
        placeholder="Nome"
        value={nome}
        onChangeText={(text) => {
          setNome(text);
        }}
      ></TextInput>
      <TextInput
        style={styles.input}
        placeholder="Telefone (Ex: 11933334444)"
        value={telefone}
        keyboardType="numeric"
        maxLength={11}
        onChangeText={(number) => {
          setTelefone(number);
        }}
      ></TextInput>
      <TextInput
        style={styles.input}
        placeholder="E-mail"
        value={email}
        onChangeText={(text) => {
          setEmail(text);
        }}
      ></TextInput>
      <TextInput
        style={styles.input}
        placeholder="Senha"
        value={senha}
        onChangeText={(text) => {
          setSenha(text);
        }}
      ></TextInput>
      <TextInput
        style={styles.input}
        placeholder="Confirmar Senha"
        value={confirmarSenha}
        onChangeText={(text) => {
          setConfirmarSenha(text);
        }}
      ></TextInput>

      {/* Botão de cadastrar que volta para a página de login */}
      <TouchableOpacity style={styles.botaoCadastrar} onPress={handleCadastrar}>
        <Text style={styles.textoBotaoCadastrar}>Cadastrar</Text>
      </TouchableOpacity>
    </View>
  );
};

//Estilização
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#fff",
    height: "100%",// ocupar altura e largura
    width: "100%",
  },
  textoCadastrar: {
    fontSize: 28,
    marginTop: 40,
    marginBottom: 60,
  },
  input: {
    borderWidth: 0.5,
    borderRadius: 3,
    width: 300,
    height: 45,
    paddingLeft: 5,
    marginBottom: 24,
  },
  botaoCadastrar: {
    width: 300,
    height: 55,
    marginTop: 40,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    borderWidth: 1,
    backgroundColor: "orange",
  },
  textoBotaoCadastrar: {
    fontSize: 20,
  },
});

export default Cadastro;
